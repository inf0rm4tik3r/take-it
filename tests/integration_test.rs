use take_it::{TakeAsTuple, TakeAtLeast, TakeExactly};

#[test]
fn take_at_least() {
    let mut it = "abc".chars().take_at_least::<2>();
    assert_eq!(Some('a'), it.next());
    assert_eq!(Some('b'), it.next());
    assert_eq!(None, it.next());
}

#[test]
fn take_at_least_as_tuple() {
    let chars = "abc".chars().take_at_least::<2>().as_tuple();
    assert_eq!('a', chars.0);
    assert_eq!('b', chars.1);
}

#[test]
fn take_exactly() {
    let mut it = "ab".chars().take_at_least::<2>();
    assert_eq!(Some('a'), it.next());
    assert_eq!(Some('b'), it.next());
    assert_eq!(None, it.next());
}

#[test]
fn take_exactly_as_tuple() {
    let chars = "ab".chars().take_exactly::<2>().as_tuple();
    assert_eq!('a', chars.0);
    assert_eq!('b', chars.1);
}
