//! This crate adds the `take_at_least` and `take_exactly` methods as an extension for iterators.
//!
//! # Example
//!
//! ```
//! use take_it::TakeAtLeast;
//!
//! let mut it = "abc".chars().take_at_least::<2>();
//! assert_eq!(Some('a'), it.next());
//! assert_eq!(Some('b'), it.next());
//! assert_eq!(None, it.next());
//! ```

mod take_at_least;
mod take_exactly;

pub use crate::take_at_least::TakeAtLeast;
pub use crate::take_exactly::TakeExactly;

mod take_as_triple;
mod take_as_tuple;

pub use crate::take_as_triple::TakeAsTriple;
pub use crate::take_as_tuple::TakeAsTuple;
