use crate::take_at_least::AtLeastConstSizeIteratorWrapper;
use crate::take_exactly::ExactConstSizeIteratorWrapper;

pub trait TakeAsTriple<T> {
    fn as_triple(&mut self) -> (T, T, T);
}

impl<I: Iterator> TakeAsTriple<I::Item> for AtLeastConstSizeIteratorWrapper<I, 3> {
    fn as_triple(&mut self) -> (I::Item, I::Item, I::Item) {
        (
            self.next().unwrap(),
            self.next().unwrap(),
            self.next().unwrap(),
        )
    }
}

impl<I: Iterator> TakeAsTriple<I::Item> for ExactConstSizeIteratorWrapper<I, 3> {
    fn as_triple(&mut self) -> (I::Item, I::Item, I::Item) {
        (
            self.next().unwrap(),
            self.next().unwrap(),
            self.next().unwrap(),
        )
    }
}

impl<I: Iterator> From<AtLeastConstSizeIteratorWrapper<I, 3>> for (I::Item, I::Item, I::Item) {
    fn from(mut it: AtLeastConstSizeIteratorWrapper<I, 3>) -> Self {
        it.as_triple()
    }
}

impl<I: Iterator> From<ExactConstSizeIteratorWrapper<I, 3>> for (I::Item, I::Item, I::Item) {
    fn from(mut it: ExactConstSizeIteratorWrapper<I, 3>) -> Self {
        it.as_triple()
    }
}

#[cfg(test)]
mod test {
    use super::TakeAsTriple;
    use crate::{TakeAtLeast, TakeExactly};

    #[test]
    fn test_at_least() {
        let chars = "abcd".chars().take_at_least::<3>().as_triple();
        assert_eq!('a', chars.0);
        assert_eq!('b', chars.1);
        assert_eq!('c', chars.2);
    }

    #[test]
    fn test_at_least_from_into() {
        let chars: (char, char, char) = "abcd".chars().take_at_least::<3>().into();
        assert_eq!('a', chars.0);
        assert_eq!('b', chars.1);
        assert_eq!('c', chars.2);
    }

    #[test]
    fn test_exactly() {
        let chars = "abc".chars().take_exactly::<3>().as_triple();
        assert_eq!('a', chars.0);
        assert_eq!('b', chars.1);
        assert_eq!('c', chars.2);
    }

    #[test]
    fn test_exactly_from_into() {
        let chars: (char, char, char) = "abc".chars().take_exactly::<3>().into();
        assert_eq!('a', chars.0);
        assert_eq!('b', chars.1);
        assert_eq!('c', chars.2);
    }
}
