use crate::take_at_least::AtLeastConstSizeIteratorWrapper;
use crate::take_exactly::ExactConstSizeIteratorWrapper;

pub trait TakeAsTuple<T> {
    fn as_tuple(&mut self) -> (T, T);
}

impl<I: Iterator> TakeAsTuple<I::Item> for AtLeastConstSizeIteratorWrapper<I, 2> {
    fn as_tuple(&mut self) -> (I::Item, I::Item) {
        (self.next().unwrap(), self.next().unwrap())
    }
}

impl<I: Iterator> TakeAsTuple<I::Item> for ExactConstSizeIteratorWrapper<I, 2> {
    fn as_tuple(&mut self) -> (I::Item, I::Item) {
        (self.next().unwrap(), self.next().unwrap())
    }
}

#[cfg(test)]
mod test {
    use super::TakeAsTuple;
    use crate::{TakeAtLeast, TakeExactly};

    #[test]
    fn test_at_least() {
        let chars = "abc".chars().take_at_least::<2>().as_tuple();
        assert_eq!('a', chars.0);
        assert_eq!('b', chars.1);
    }

    #[test]
    fn test_exactly() {
        let chars = "abc".chars().take_exactly::<2>().as_tuple();
        assert_eq!('a', chars.0);
        assert_eq!('b', chars.1);
    }
}
