pub trait TakeAtLeast: Iterator + Sized {
    /// Take exactly x elements, promoting the iterator into an exact size iterator.
    /// The `length` specified must be the exact length of the iterator (amount of items left) or the program will panic!
    fn take_at_least<const S: usize>(self) -> AtLeastConstSizeIteratorWrapper<Self, S> {
        AtLeastConstSizeIteratorWrapper {
            iterator: self,
            remaining: S,
        }
    }

    fn take_at_least_var(self, length: usize) -> AtLeastVarSizeIteratorWrapper<Self> {
        AtLeastVarSizeIteratorWrapper {
            iterator: self,
            remaining: length,
        }
    }
}

impl<I: Iterator> TakeAtLeast for I {}

pub struct AtLeastConstSizeIteratorWrapper<I: Iterator, const S: usize> {
    iterator: I,
    remaining: usize,
}

pub struct AtLeastVarSizeIteratorWrapper<I: Iterator> {
    iterator: I,
    remaining: usize,
}

impl<I: Iterator, const S: usize> Iterator for AtLeastConstSizeIteratorWrapper<I, S> {
    type Item = I::Item;

    fn next(&mut self) -> Option<Self::Item> {
        // Return early when finished, ensuring that we do not call .next() on the underlying iterator if we do not have to.
        if self.remaining == 0 {
            return None;
        }
        let next = self.iterator.next();
        if next.is_none() && self.remaining != 0 {
            panic!("TakeExactly: ERROR: {} additional elements were expected to be consumed! Iterator yielded too few elements.", self.remaining);
        }
        self.remaining -= 1;
        next
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        (self.remaining, None)
    }
}

// TODO: Duplicate implementation using a macro?
impl<I: Iterator> Iterator for AtLeastVarSizeIteratorWrapper<I> {
    type Item = I::Item;

    fn next(&mut self) -> Option<Self::Item> {
        // Return early when finished, ensuring that we do not call .next() on the underlying iterator if we do not have to.
        if self.remaining == 0 {
            return None;
        }
        let next = self.iterator.next();
        if next.is_none() && self.remaining != 0 {
            panic!("TakeExactly: ERROR: {} additional elements were expected to be consumed! Iterator yielded too few elements.", self.remaining);
        }
        self.remaining -= 1;
        next
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        (self.remaining, None)
    }
}

impl<I: Iterator, const S: usize> ExactSizeIterator for AtLeastConstSizeIteratorWrapper<I, S> {}

#[cfg(test)]
mod test_take_at_least {
    use crate::TakeAtLeast;

    #[test]
    fn when_underlying_iter_yields_exact_amount_of_elements_then_does_not_panic() {
        let mut it = "ab".chars().take_at_least::<2>();
        assert_eq!(Some('a'), it.next());
        assert_eq!(Some('b'), it.next());
        assert_eq!(None, it.next());
    }

    #[test]
    fn when_underlying_iter_yields_more_elements_then_does_not_panic() {
        let mut it = "abc".chars().take_at_least::<2>();
        assert_eq!(Some('a'), it.next());
        assert_eq!(Some('b'), it.next());
        assert_eq!(None, it.next());
    }

    #[test]
    #[should_panic(
        expected = "TakeExactly: ERROR: 1 additional elements were expected to be consumed! Iterator yielded too few elements."
    )]
    fn when_underlying_iter_can_not_yield_enough_elements_then_panic() {
        let mut it = "a".chars().take_at_least::<2>();
        assert_eq!(Some('a'), it.next());
        it.next(); // <- Panics, as 1 additional element is expected and none can be given from "a".chars().
    }
}

#[cfg(test)]
mod test_take_at_least_var {
    use crate::TakeAtLeast;

    #[test]
    fn when_underlying_iter_yields_exact_amount_of_elements_then_does_not_panic() {
        let mut it = "ab".chars().take_at_least_var(2);
        assert_eq!(Some('a'), it.next());
        assert_eq!(Some('b'), it.next());
        assert_eq!(None, it.next());
    }

    #[test]
    fn when_underlying_iter_yields_more_elements_then_does_not_panic() {
        let mut it = "abc".chars().take_at_least_var(2);
        assert_eq!(Some('a'), it.next());
        assert_eq!(Some('b'), it.next());
        assert_eq!(None, it.next());
    }

    #[test]
    #[should_panic(
        expected = "TakeExactly: ERROR: 1 additional elements were expected to be consumed! Iterator yielded too few elements."
    )]
    fn when_underlying_iter_can_not_yield_enough_elements_then_panic() {
        let mut it = "a".chars().take_at_least_var(2);
        assert_eq!(Some('a'), it.next());
        it.next(); // <- Panics, as 1 additional element is expected and none can be given from "a".chars().
    }
}
