pub trait TakeExactly: Iterator + Sized {
    /// Take exactly x elements, promoting the iterator into an exact size iterator.
    /// The `length` specified must be the exact length of the iterator (amount of items left) or the program will panic!
    fn take_exactly<const S: usize>(self) -> ExactConstSizeIteratorWrapper<Self, S> {
        ExactConstSizeIteratorWrapper {
            iterator: self,
            remaining: S,
        }
    }
    
    fn take_exactly_var(self, length: usize) -> ExactVarSizeIteratorWrapper<Self> {
        ExactVarSizeIteratorWrapper {
            iterator: self,
            remaining: length,
        }
    }
}

impl<I: Iterator> TakeExactly for I {}

pub struct ExactConstSizeIteratorWrapper<I: Iterator, const S: usize> {
    iterator: I,
    remaining: usize,
}

pub struct ExactVarSizeIteratorWrapper<I: Iterator> {
    iterator: I,
    remaining: usize,
}

impl<I: Iterator, const S: usize> Iterator for ExactConstSizeIteratorWrapper<I, S> {
    type Item = I::Item;

    fn next(&mut self) -> Option<Self::Item> {
        let next = self.iterator.next();
        if next.is_none() {
            if self.remaining > 0 {
                panic!("TakeExactly: ERROR: {} additional elements were expected to be consumed! Iterator yielded too few elements.", self.remaining);
            }
        } else {
            if self.remaining == 0 {
                panic!("TakeExactly: ERROR: Some elements were not consumed!");
            }
            self.remaining -= 1;
        }
        next
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        (self.remaining, Some(self.remaining))
    }
}

impl<I: Iterator> Iterator for ExactVarSizeIteratorWrapper<I> {
    type Item = I::Item;

    fn next(&mut self) -> Option<Self::Item> {
        let next = self.iterator.next();
        if next.is_none() {
            if self.remaining > 0 {
                panic!("TakeExactly: ERROR: {} additional elements were expected to be consumed! Iterator yielded too few elements.", self.remaining);
            }
        } else {
            if self.remaining == 0 {
                panic!("TakeExactly: ERROR: Some elements were not consumed!");
            }
            self.remaining -= 1;
        }
        next
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        (self.remaining, Some(self.remaining))
    }
}


impl<I: Iterator, const S: usize> ExactSizeIterator for ExactConstSizeIteratorWrapper<I, S> {}
impl<I: Iterator> ExactSizeIterator for ExactVarSizeIteratorWrapper<I> {}

#[cfg(test)]
mod test_take_exactly {
    use crate::TakeExactly;

    #[test]
    fn when_underlying_iter_yields_exact_amount_of_elements_then_does_not_panic() {
        let mut it = "ab".chars().take_exactly::<2>();
        assert_eq!(Some('a'), it.next());
        assert_eq!(Some('b'), it.next());
        assert_eq!(None, it.next());
    }

    #[test]
    #[should_panic(expected = "TakeExactly: ERROR: Some elements were not consumed!")]
    fn when_underlying_iter_yields_more_elements_then_panics() {
        let mut it = "abc".chars().take_exactly::<2>();
        assert_eq!(Some('a'), it.next());
        assert_eq!(Some('b'), it.next());
        it.next(); // <- Panics, as no more elements are expected but "abc".chars() will yield an additional char.
    }

    #[test]
    #[should_panic(
        expected = "TakeExactly: ERROR: 1 additional elements were expected to be consumed! Iterator yielded too few elements."
    )]
    fn when_underlying_iter_can_not_yield_enough_elements_then_panic() {
        let mut it = "a".chars().take_exactly::<2>();
        assert_eq!(Some('a'), it.next());
        it.next(); // <- Panics, as 1 additional element is expected and none can be given from "a".chars().
    }
}

#[cfg(test)]
mod test_take_exactly_var {
    use crate::TakeExactly;

    #[test]
    fn when_underlying_iter_yields_exact_amount_of_elements_then_does_not_panic() {
        let mut it = "ab".chars().take_exactly_var(2);
        assert_eq!(Some('a'), it.next());
        assert_eq!(Some('b'), it.next());
        assert_eq!(None, it.next());
    }

    #[test]
    #[should_panic(expected = "TakeExactly: ERROR: Some elements were not consumed!")]
    fn when_underlying_iter_yields_more_elements_then_panics() {
        let mut it = "abc".chars().take_exactly_var(2);
        assert_eq!(Some('a'), it.next());
        assert_eq!(Some('b'), it.next());
        it.next(); // <- Panics, as no more elements are expected but "abc".chars() will yield an additional char.
    }

    #[test]
    #[should_panic(
        expected = "TakeExactly: ERROR: 1 additional elements were expected to be consumed! Iterator yielded too few elements."
    )]
    fn when_underlying_iter_can_not_yield_enough_elements_then_panic() {
        let mut it = "a".chars().take_exactly_var(2);
        assert_eq!(Some('a'), it.next());
        it.next(); // <- Panics, as 1 additional element is expected and none can be given from "a".chars().
    }
}
